# HBSIS-Desafios-Par

Exercícios em dupla do programa Padawan da HBSIS, onde a dupla se deu por Claudia e Louyza, padawans de python.

Os exercícios a serem feitos na semana do dia 06/01/2020 à 10/01/2020 foram os PADs 46 à 54 e o 62. 

# PAD 46 ------------

Descrição:
Você receberá uma palavra. Seu trabalho é retornar o caractere do meio da palavra.
Se o comprimento da palavra for ímpar, retorne o caractere do meio.
Se o comprimento da palavra for par, retorne os 2 caracteres do meio.

Exemplos:
Kata.getMiddle("test") should return "es"
Kata.getMiddle("testing") should return "t"
Kata.getMiddle("middle") should return "dd"
Kata.getMiddle("A") should return "A"

# PAD 47 ------------

Descrição:
Desta vez, nenhuma história, nenhuma teoria. Os exemplos abaixo mostram como escrever a função accum:

Exemplos:
accum("abcd") -> "A-Bb-Ccc-Dddd"
accum("RqaEzty") -> "R-Qq-Aaa-Eeee-Zzzzz-Tttttt-Yyyyyyy"
accum("cwAt") -> "C-Ww-Aaa-Tttt"
O parâmetro acum é uma string que inclui apenas letras de a..z e A..Z.

# PAD 48 ------------

Retorne o número (contagem) de vogais na sequência especificada.

Vamos considerar a, e, i, o e u como vogais para este Kata.

A sequência de entrada consistirá apenas de letras minúsculas e / ou espaços.

# PAD 49 ------------

Simples, com uma sequência de palavras,
retorne o comprimento da(s) palavra(s) mais curta(s).

A sequência nunca estará vazia e você não precisará contabilizar
diferentes tipos de dados.

# PAD 50 ------------

Descrição
Sua tarefa é criar uma função que possa pegar qualquer número inteiro não negativo como argumento
e retorná-lo com seus dígitos em ordem decrescente. Essencialmente, reorganize os dígitos para criar o
número mais alto possível.

Exemplos:
Entrada: 21445 Saída:54421
Entrada: 145263 Saída:654321
Entrada: 123456789 Saída:987654321

# PAD 51 ------------

Descrição:
Neste desafio, você é solicitado a colocar todos os dígitos de um número ao quadrado.
Por exemplo, se executarmos 9119 por meio da função, 811181 sairá, porque 9 2 é 81 e 1 2 é 1.

Nota: A função aceita um número inteiro e retorna um número inteiro

# PAD 52 ------------

Você receberá uma matriz. Os valores na matriz serão números ou
seqüências de caracteres, ou uma mistura de ambos.

Você não receberá uma matriz vazia, nem uma dispersa.

Seu trabalho é retornar uma única matriz que tenha primeiro os números classificados em ordem crescente, 
seguidos pelas cadeias classificadas em ordem alfabética. Os valores devem manter seu tipo original.

Observe que os números escritos como strings são strings e devem ser classificados com as outras strings.

# PAD 53 ------------

Descrição:
A cada dia uma planta cresce em upSpeedmetros. Todas as noites a altura da planta diminui
em downSpeedmetros devido à falta de calor do sol. Inicialmente, a planta tem 0 metros de altura.
Plantamos a semente no início de um dia. Queremos saber quando a altura da planta atingirá um certo nível.

Exemplo:
Pois upSpeed = 100, downSpeed = 10 and desiredHeight = 910, a saída deve ser 10.

 After day 1 --> 100
 After night 1 --> 90
 After day 2 --> 190
 After night 2 --> 180
 After day 3 --> 280
 After night 3 --> 270
 After day 4 --> 370
 After night 4 --> 360
 After day 5 --> 460
 After night 5 --> 450
 After day 6 --> 550
 After night 6 --> 540
 After day 7 --> 640
 After night 7 --> 630
 After day 8 --> 730
 After night 8 --> 720
 After day 9 --> 820
 After night 9 --> 810
 After day 10 --> 910
Pois upSpeed = 10, downSpeed = 9 and desiredHeight = 4, a saída deve ser 1.

Porque a planta atinge a altura desejada no dia 1 (10 metros).

After day 1 --> 10
Entrada / Saída
[input] inteiro upSpeed

Um número inteiro positivo que representa o crescimento diário.

Restrições: 5 ≤ upSpeed ≤ 100.

[input] inteiro downSpeed

Um número inteiro positivo representando o declínio noturno.

Restrições: 2 ≤ downSpeed < upSpeed.

[input] inteiro desiredHeight

Um número inteiro positivo que representa o limite.

Restrições: 4 ≤ desiredHeight ≤ 1000.

[output] um inteiro

O número de dias que a planta alcançará / ultrapassará a Altura desejada (incluindo o último dia na contagem total).

# PAD 54 ------------

Dois navios de pesca navegam em mar aberto, ambos em uma missão conjunta de pesca.

Em altas apostas, alta expectativa de recompensa - os navios adotaram a estratégia de
pendurar uma rede entre os dois navios.

A rede tem 40 milhas de comprimento . Quando a distância em linha reta entre os navios for superior a 64 km,
a rede rasgará e sua valiosa colheita no mar será perdida! Precisamos saber quanto tempo levará para que isso
aconteça.

Dado o rumo de cada navio, encontre o tempo em minutos em que a distância em linha reta entre
os dois navios atinge 40 milhas .
Ambos os navios viajam a 150 quilômetros por hora . No momento 0, suponha que os navios tenham o mesmo local.

Os rolamentos são definidos como graus do norte, contando no sentido horário . Eles serão passados
para sua função como
números inteiros entre 0 e 359 graus. Arredonde seu resultado para 2 locais decimais .

Se a rede nunca quebrar, retorne float('inf')

# PAD 62 ------------

Descrição:
Bob está se preparando para passar no teste de QI. A tarefa mais frequente neste teste
é to find out which one of the given numbers differs from the others. Bob observou que um número geralmente
difere dos outros na igualdade .

Ajude Bob - para verificar suas respostas, ele precisa de um programa que, dentre os números indicados, encontre
um que seja diferente na uniformidade e retorne uma posição desse número.

Lembre-se de que sua tarefa é ajudar Bob a resolver a real IQ test, o que significa que os índices
dos elementos começam em 1 (not 0)

Exemplos :
IQ.Test("2 4 7 8 10") => 3 // Third number is odd, while the rest of the numbers are even
IQ.Test("1 2 1 1") => 2 // Second number is even, while the rest of the numbers are odd

lista = [2, 4, 7, 8, 10]
lista2 = [1, 2, 1, 1]


# PAD 64 ------------

Descrição:
Dado uma lista (arr) como argumento, conclua a função count_smileys que deve retornar o número
total de rostos sorridentes.

Regras para um rosto sorridente:
Cada rosto sorridente deve conter um par de olhos válido. Os olhos podem ser marcados como : ou ;

Um rosto sorridente pode ter nariz, mas não precisa. Os caracteres válidos para um nariz são - ou ~

Todo rosto sorridente deve ter uma boca sorridente que deve ser marcada com um ) ou outro D.

Nenhum caractere adicional é permitido, exceto os mencionados.
Exemplos válidos de carinhas

:) :D ;-D :~)

Carinhas inválidas:

;( :> :} :]

casos de exemplo:

count_smileys([':)', ';(', ';}', ':-D']); // Deve retornar 2;
count_smileys([';D', ':-(', ':-)', ';~)']); // Deve retornar 3;
count_smileys([';]', ':[', ';*', ':$', ';-D']); // Deve retornar 1;

Nota: No caso de uma lista vazia, retorne 0. Você não será testado com entrada inválida (a entrada sempre será uma lista). A ordem dos elementos do rosto (olhos, nariz, boca) sempre será a mesma









